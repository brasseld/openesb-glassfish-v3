package org.openesb.glassfish3.jbi;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import org.glassfish.api.admin.ServerEnvironment;
import org.glassfish.internal.api.Globals;

import com.sun.enterprise.config.serverbeans.Cluster;
import com.sun.enterprise.config.serverbeans.Domain;
import com.sun.enterprise.config.serverbeans.Server;
import com.sun.enterprise.util.SystemPropertyConstants;
import com.sun.logging.LogDomains;

/**
 * The JBI Runtime needs information on the servers/clusters in a domain, state
 * of an instance etc. This is the interface to get this application server context
 * information.
 * 
 * @author David BRASSELY
 */
public class AppServerContextImpl implements AppServerContext {

	private static String SERVER = SystemPropertyConstants.DEFAULT_SERVER_INSTANCE_NAME;

	private static Logger _logger = Logger.getLogger(LogDomains.SERVER_LOGGER);

	private Method irMethod = null;

	private final String INSTANCE_REGISTRY = "com.sun.enterprise.ee.admin.clientreg.InstanceRegistry";
	
	private final ServerEnvironment env;
	
	private final Domain domain;

	public AppServerContextImpl() {
		env = Globals.getDefaultHabitat().getComponent(ServerEnvironment.class);
		domain = Globals.getDefaultHabitat().getComponent(Domain.class);
		
		// TODO: should we use this privileged action ? 
		/*
		try {
			irMethod = (Method) java.security.AccessController.doPrivileged(
					new java.security.PrivilegedExceptionAction() {
						public java.lang.Object run() throws Exception {
							Class cls = Class.forName(INSTANCE_REGISTRY);
							return cls.getMethod("getInstanceConnection",
									new Class[] { String.class });
						}
					});
		} catch (Exception e) {
			_logger.log(Level.SEVERE, e.getMessage(), e);
			throw new RuntimeException(e.getMessage());
		}
		*/
	}

	/*----------------------------------------------------------------------------------*\

	 *         Common operations invoked from the DAS and non-DAS instances.            *

	\*----------------------------------------------------------------------------------*/

	/**
	 * 
	 * Determine if this instance is the central administration server.
	 * 
	 * @return true if this instance is the central administration server.
	 */
	public boolean isDAS() {
		return env.isDas();
	}

	/**
	 * 
	 * Get the "target" for this instance. If this instance :
	 * 
	 * &lt;ul>
	 * 
	 * &lt;li> is the central admininistration server instance then target =
	 * "server" &lt;/li>
	 * 
	 * &lt;li> is a clustered instance then target = [cluster-name] &lt;/li>
	 * 
	 * &lt;li> is a standalone instance then target = [server-name] &lt;/li>
	 * 
	 * &lt;/ul>
	 * 
	 * &lt;br/>
	 * 
	 * This is equivalent to calling getTargetName(getInstanceName()).
	 * 
	 * 
	 * 
	 * @return the "target" for this instance.
	 */
	public String getTargetName() throws Exception {
		String instanceName = getInstanceName();
		return getTargetName(instanceName);
	}

	/**
	 * 
	 * Get the name of the server instance. In case this is the central
	 * administration server then instance name = "server".
	 * 
	 * @return the name of the glassfish instance.
	 */
	public String getInstanceName() {
		//TODO : To be checked
		return env.getInstanceName();
		// return System.getProperty("com.sun.aas.instanceName");
	}

	/**
	 * 
	 * Get the MBeanServerConnection for a specific instance.
	 * 
	 * @param instanceName
	 *            - instance name, the MBeanServerConnection for which is to be obtained.
	 * 
	 * @return the MBeanServerConnection to a given instance.
	 * 
	 * @throws Exception
	 *             on errors
	 */
	public MBeanServerConnection getMBeanServerConnection(final String instanceName) throws Exception {
		
		// If the instance name is the same as the local instance return the local MBeanServer
		if (getInstanceName().equals(instanceName)) {
			return getPlatformMBeanServer();
		}

		if (isDAS() && !multipleServersSupported()) {
			// Developer profile
			throw new Exception("Developer profile does not support multiple server instances");

		} else {
			return (MBeanServerConnection) java.security.AccessController.doPrivileged(
					new java.security.PrivilegedExceptionAction() {
						public java.lang.Object run() throws Exception {
							return irMethod.invoke(null, new Object[] { instanceName });
						}
					});
		}
	}

	/*----------------------------------------------------------------------------------*\

	 *         Operations invoked from the DAS only.                                    *

	\*----------------------------------------------------------------------------------*/

	/**
	 * 
	 * Determine the runtime state of an instance.
	 * 
	 * @param instanceName
	 *            - name of the instance whose state is to be determined.
	 * 
	 * @return true if the specified instance is running false is it is shutdown
	 */
	public  boolean isInstanceUp(String instanceName) {
        boolean isRunning = false;

        if ( isDAS() ) {

            if ( SERVER.equals(instanceName)) {
                // -- If DAS is running, so is the "server" instance.
                isRunning = true;
            } else {
                String instanceObjName = "amx:J2EEServer=" + instanceName + ",j2eeType=JVM,*";
                ObjectName objName = null;

                try {
                	objName =  new ObjectName(instanceObjName);
                } catch(javax.management.MalformedObjectNameException mex) {
                    _logger.log(Level.SEVERE, mex.getMessage(), mex);
                    return false;
                }

                Set<ObjectName> nameSet = getPlatformMBeanServer().queryNames(objName, null);

                if ( (!nameSet.isEmpty()) ) {
                    isRunning = true;
                }
            }
        }

        return isRunning;
    }

	/**
	 * 
	 * Determine if the central administration server supports multiple servers.
	 * 
	 * @return true if this administration server supports multiple servers.
	 */
	public boolean multipleServersSupported() {
		return isDAS();
		/*
		if (isDAS()) {
			MBeanServer mbeanServer = java.lang.management.ManagementFactory
					.getPlatformMBeanServer();

			DomainRoot domainRoot = ProxyFactory.getInstance(mbeanServer).getDomainRoot();

			return domainRoot.getSystemInfo().supportsFeature(
					SystemInfo.MULTIPLE_SERVERS_FEATURE);
		} else {
			return false;
		}
		*/
	}

	/**
	 * 
	 * Get the names of all the non-clustered standalone servers in the domain.
	 * 
	 * @return an array of names of all the standalone servers in the domain.
	 */
	public String[] getStandaloneServerNames() {
		try {
			List<Server> servers = getUnclusteredServers();
			return getServerNames(servers);
		} catch (Exception ex) {
			_logger.log(Level.WARNING, ex.getMessage(), ex);
			return new String[0];
		}
	}

	/**
	 * 
	 * Get the names of all the clusters in the domain.
	 * 
	 * @return an array of names of all the clusters in the domain. If the
	 *         domain has zero clusters then an empty array is returned.
	 */
	public String[] getClusterNames() {
		try {
			List<Cluster> clusters = domain.getClusters().getCluster();

			return getClusterNames(clusters);
		} catch (Exception ex) {
			_logger.log(Level.WARNING, ex.getMessage(), ex);
			return new String[0];
		}
	}

	/**
	 * 
	 * Get the names of all the member servers in the specified cluster.
	 * 
	 * 
	 * 
	 * @return an array of names of all the servers in the cluster. If the clusterName is a 
	 *         non-existent cluster or does not have any member instances then an empty
	 *         array is returned.
	 */
	public String[] getServersInCluster(String clusterName) {
		try {
			List<Server> servers = domain.getServersInTarget(clusterName);
			// Server[] servers = ServerHelper.getServersInCluster(getConfigContext(), clusterName);
			return getServerNames(servers);
		} catch (Exception ex) {
			_logger.log(Level.WARNING, ex.getMessage(), ex);
			return new String[0];
		}
	}

	/**
	 * 
	 * Get the "target" for the specified instance.
	 * 
	 * 
	 * 
	 * &lt;ul>
	 * 
	 * &lt;li> is the central administration server instance then target =
	 * "server" &lt;/li>
	 * 
	 * &lt;li> is a clustered instance then target = [cluster-name] &lt;/li>
	 * 
	 * &lt;li> is a standalone instance then target = [server-name] &lt;/li>
	 * 
	 * &lt;/ul>
	 * 
	 * 
	 * 
	 * @param instanceName
	 *            - name of the instance, whose target is to be determined
	 * 
	 * @return the "target" for the specific instance
	 */
	public String getTargetName(String instanceName) throws Exception {
		String targetName = instanceName;
		
		if (isInstanceClustered(instanceName)) {
			// -- Target is the cluster name
			targetName = getClusterForInstance(instanceName);
		}

		return targetName;
	}

	/**
	 * 
	 * 
	 * 
	 * @return true if the instance is clustered
	 * 
	 * @param instanceName
	 *            - instance name
	 */
	public boolean isInstanceClustered(String instanceName) throws Exception {
		Server server = domain.getServerNamed(instanceName);
		return server.isCluster();
	}

	/*----------------------------------------------------------------------------------*\

	 *         Private helpers                                                          *

	\*----------------------------------------------------------------------------------*/

	/**
	 * 
	 * @return the name of the cluster the instance belongs to. If the instance
	 *         does not belong to a cluster a null is returned.
	 */
	private String getClusterForInstance(String instanceName) throws Exception {
		return domain.getClusterForInstance(instanceName).getName();
		
		/*
		String clusterName = null;
		
		if (isInstanceClustered(instanceName)) {
			Cluster cluster = ClusterHelper.getClusterForInstance(getConfigContext(), instanceName);
			clusterName = cluster.getName();
		}

		return clusterName;
		*/
	}

	/**
	 * 
	 * @return the DAS MBeanServer
	 */
	private MBeanServer getPlatformMBeanServer() {
		return java.lang.management.ManagementFactory.getPlatformMBeanServer();
	}

	/**
	 * 
	 * 
	 * 
	 * @return a Set containing the server names.
	 */
	private String[] getServerNames(List<Server> servers) {
		String[] ucServers = new String[servers.size()];
		for (int i = 0; i < servers.size(); i++) {
			ucServers[i] = servers.get(i).getName();
		}

		return ucServers;
	}

	/**
	 * 
	 * @return a String array containing the cluster names.
	 */
	private String[] getClusterNames(List<Cluster> clusters) {

		String[] domClusters = new String[clusters.size()];
		for (int i = 0; i < clusters.size(); i++) {
			domClusters[i] = clusters.get(i).getName();
		}

		return domClusters;
	}
	
	private List<Server> getUnclusteredServers() {
		List<Server> unclusteredServers = new ArrayList<Server>();
		
		for(Server server : domain.getServers().getServer()) {
			if (! server.isCluster()) {
				unclusteredServers.add(server);
			}
		}
		
		return unclusteredServers;
	}
}
