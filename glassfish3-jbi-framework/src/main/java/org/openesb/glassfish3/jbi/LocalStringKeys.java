/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.glassfish3.jbi;

/**
 * This interface contains the property keys used for looking up message
 * text in the LocalStrings resource bundle.
 *
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{ 
    /**
     * Failed to read appserver config.
     */
    String APPSERVER_CONFIG_ERROR =
          "APPSERVER_CONFIG_ERROR";
    
    /**
     * Failed to write SOAP config file.
     */
    String HTTP_SOAP_CONFIG_WRITE_ERROR =
          "HTTP_SOAP_CONFIG_WRITE_ERROR";
    
    /**
     * Failed to read SOAP config file.
     */
    String HTTP_SOAP_CONFIG_READ_ERROR =
          "HTTP_SOAP_CONFIG_READ_ERROR";
    
    /**
     * Synchronization disabled.
     */
    String JBI_SYNC_DISABLED =
            "JBI_SYNC_DISABLED";

    /**
     * Getting XA resource list for recovery processing.
     */
    String GET_XA_RESOURCES =
            "GET_XA_RESOURCES";

    /**
     * Synchronization messages.
     */
    static final String JBI_SYNC_FAILED =
        "JBI_SYNC_FAILED";
    static final String JBI_SYNC_CHECK_FAILED =
        "JBI_SYNC_CHECK_FAILED";
    static final String JBI_SYNC_FILE_COPY_FAILED =
        "JBI_SYNC_FILE_COPY_FAILED";
    static final String JBI_SYNC_CONFIG_DIFF =
        "JBI_SYNC_CONFIG_DIFF";
    static final String JBI_SYNC_ARCHIVE =
        "JBI_SYNC_ARCHIVE";
    static final String JBI_SYNC_UNINSTALL_COMP =
        "JBI_SYNC_UNINSTALL_COMP";
    static final String JBI_SYNC_INSTALL_COMP =
        "JBI_SYNC_INSTALL_COMP";
    static final String JBI_SYNC_GET_COMP_STATE =
        "JBI_SYNC_GET_COMP_STATE";
    static final String JBI_SYNC_SET_COMP_STATE =
        "JBI_SYNC_SET_COMP_STATE";
    static final String JBI_SYNC_UNINSTALL_SL =
        "JBI_SYNC_UNINSTALL_SL";
    static final String JBI_SYNC_INSTALL_SL =
        "JBI_SYNC_INSTALL_SL";
    static final String JBI_SYNC_UNDEPLOY_SA =
        "JBI_SYNC_UNDEPLOY_SA";
    static final String JBI_SYNC_DEPLOY_SA =
        "JBI_SYNC_DEPLOY_SA";
    static final String JBI_SYNC_GET_SA_STATE =
        "JBI_SYNC_GET_SA_STATE";
    static final String JBI_SYNC_SET_SA_STATE =
        "JBI_SYNC_SET_SA_STATE";
    static final String JBI_SYNC_SET_COMP_CONFIGURATION =
        "JBI_SYNC_SET_COMP_CONFIGURATION";
    static final String JBI_SYNC_DAS_NOT_AVAILABLE =
         "JBI_SYNC_DAS_NOT_AVAILABLE";
    static final String JBI_SYNC_DAS_FAILURE =
         "JBI_SYNC_DAS_FAILURE";
    static final String JBI_SYNC_HTTP_SOAP_CONFIG_READ_ERROR =
        "JBI_SYNC_HTTP_SOAP_CONFIG_READ_ERROR";
    static final String JBI_SYNC_APPSERVER_CONFIG_ERROR =
        "JBI_SYNC_APPSERVER_CONFIG_ERROR";
    static final String JBI_SYNC_HTTP_SOAP_CONFIG_WRITE_ERROR =
        "JBI_SYNC_HTTP_SOAP_CONFIG_WRITE_ERROR";
    static final String JBI_SYNC_ADD_APP_VAR =
        "JBI_SYNC_ADD_APP_VAR";
    static final String JBI_SYNC_REMOVE_APP_VAR =
        "JBI_SYNC_REMOVE_APP_VAR";
    static final String JBI_SYNC_ADD_APP_CONFIG =
        "JBI_SYNC_ADD_APP_CONFIG";
    static final String JBI_SYNC_REMOVE_APP_CONFIG =
        "JBI_SYNC_REMOVE_APP_CONFIG";
    static final String JBI_SYNC_SET_COMP_PROP =
        "JBI_SYNC_SET_COMP_PROP";
    static final String JBI_SYNC_APPLY_CONFIGURATION =
        "JBI_SYNC_APPLY_CONFIGURATION";

    /**
     * Exception in a system component during bootstrap.
     */
    String SYSTEM_COMPONENT_INIT_EXCEPTION =
            "SYSTEM_COMPONENT_INIT_EXCEPTION";
}
