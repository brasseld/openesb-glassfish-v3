package org.openesb.glassfish3.jbi;

import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.JBIException;
import javax.naming.InitialContext;
import javax.transaction.xa.XAResource;

import org.glassfish.api.event.EventListener;
import org.glassfish.api.event.EventTypes;
import org.glassfish.internal.api.Globals;

import com.sun.enterprise.transaction.spi.RecoveryResourceListener;
import com.sun.jbi.StringTranslator;

/**
 * This is the top-level class that provides the lifecycle for the JBI
 * framework. It implements the Sun AppServer <CODE>LifecycleListener</CODE>
 * interface to allow the JBI runtime framework to start automatically when the
 * AppServer starts, and stop automatically when the AppServer stops. The one
 * method required by the <CODE>LifecycleListener</CODE> interface is
 * <CODE>handleEvent()</CODE>, which handles AppServer life cycle events for
 * initialization, startup, ready, shutdown, and termination. Each event is
 * handled by a corresponding method in this class, as follows:
 * 
 * <UL>
 * <LI>INIT_EVENT ---> <CODE>init()</CODE></LI>
 * <LI>STARTUP_EVENT ---> <CODE>startup()</CODE></LI>
 * <LI>READY_EVENT ---> <CODE>ready()</CODE></LI>
 * <LI>SHUTDOWN_EVENT ---> <CODE>shutdown()</CODE></LI>
 * <LI>TERMINATION_EVENT ---> <CODE>terminate()</CODE></LI>
 * </UL>
 * 
 * This class is loaded by the <CODE>SunASJBIBootstrap</CODE> class, which also
 * implements the <CODE>LifecycleListener</CODE> interface. All events passed to
 * its <handleEvent()</CODE> method are merely passed through to the
 * <CODE>handleEvent()</CODE> method of this class, with one exception. When the
 * INIT_EVENT is received, the required class loader hierarchy for the JBI
 * runtime is established prior to passing the event through. The reason for
 * this is to ensure that the JBI runtime implementation classes are isolated
 * from the class loaders that are created for installed Components and Shared
 * Libraries. For further information, please refer to the Javadoc for the
 * <CODE>SunASJBIBootstrap</CODE> class.
 * 
 * @author Sun Microsystems, Inc.
 */
public class SunASJBIFramework extends com.sun.jbi.framework.JBIFramework implements EventListener,
		Runnable, RecoveryResourceListener {
	private static final String STRING_TRANSLATOR_NAME = "com.sun.jbi.framework.sun";

	/**
	 * Glassfish platform details.
	 */
	private SunASPlatformContext mPlatform;

	/**
	 * JBI Home property, which is the install root.
	 */
	private static final String JBI_HOME = "com.sun.jbi.home";

	/**
	 * Glassfish Instance Root folder system variable
	 */
	private static final String INSTALL_ROOT = "com.sun.aas.installRoot";

	/**
	 * Sync thread. Will be running if synchronization is required until
	 * completed.
	 */
	private Thread mSyncThread;
	private RegistryHelper mRegistryHelper;
	private SyncProcessor mSyncProcessor;
	private Logger mLogger;
	private StringTranslator mTranslator;

	private static final String SERVER_INIT_NAME = "server_init";
	private static final EventTypes<?> SERVER_INIT = EventTypes.create(SERVER_INIT_NAME);
	
	/**
	 * Last event received.
	 */
	private EventTypes<?> mLastEvent;

	private long mStartStamp;
	private long mStartTime;

	/**
	 * Tracks the current phase of the Framework. This is used to block activity
	 * that depends on being in a particular phase.
	 */
	private int mPhase;
	private static final int PHASE_NONE = 0;
	private static final int PHASE_INIT = 1;
	private static final int PHASE_STARTUP = 2;
	private static final int PHASE_PREPARE = 3;
	private static final int PHASE_READY = 4;
	private static final int PHASE_SHUTDOWN = 5;
	private static final int PHASE_TERMINATE = 6;

	public SunASJBIFramework() {
		this.init();
	}

	private void init() {
		try {
			mStartStamp = System.currentTimeMillis();
			mPlatform = new SunASPlatformContext();
			mPhase = PHASE_NONE;

			Properties props = new Properties();
			
			String installRoot = System.getProperty( INSTALL_ROOT );
			String jbiRoot = installRoot + File.separator + "jbi";
			
			props.put(JBI_HOME, jbiRoot);

			super.init(mPlatform, props);

			mTranslator = getEnvironment().getStringTranslator(STRING_TRANSLATOR_NAME);
			mLogger = Logger.getLogger("com.sun.jbi.framework");
			mRegistryHelper = new RegistryHelper(getEnvironment());
			mSyncProcessor = new SyncProcessor(getEnvironment(), mRegistryHelper);
			endPhase(PHASE_INIT);
			mLogger.fine("JBI:Start Time 0: " + (System.currentTimeMillis() - mStartStamp));
		} catch (JBIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void event(final Event anEvent) {

		try {
			if (EventTypes.SERVER_STARTUP.equals(anEvent.type())) {
				if (null != getEnvironment()) {
					InitialContext context = Globals.getDefaultHabitat().getComponent(
							InitialContext.class);
					//TODO: get default initial context
					mPlatform.setNamingContext(context); // eventContext.getInitialContext());
					super.startup(mPlatform.getNamingContext(), "");
					endPhase(PHASE_STARTUP);
					mLogger.fine("JBI:Start Time 1: " + (System.currentTimeMillis() - mStartStamp));
				}
			} else if (EventTypes.SERVER_READY.equals(anEvent.type())) {
				if (null != getEnvironment()) {
					long startTime = System.currentTimeMillis();
					// Perform intial processing (e.g. config) for system
					// components
					bootstrapSystemComponents();

					//
					//
					boolean isStartupActive = mRegistryHelper.isActiveStartup();
					mStartTime += System.currentTimeMillis() - startTime;
					if (isStartupActive) {
						//
						// If we are going from STARTUP to READY, synthesize a
						// PREPARE so that the underlying code doesn't have to
						// track the AS-specific behavior,
						//
						if (mLastEvent == EventTypes.SERVER_STARTUP) {
							mLogger.fine("JBI:ActiveStartup");
							super.prepare();
							endPhase(PHASE_PREPARE);
						}
						ready();
						endPhase(PHASE_READY);
					} else {
						enterLazyMode();
					}
				}
				mLogger.fine("JBI:Start Time 2: " + (System.currentTimeMillis() - mStartStamp));
			} else if (EventTypes.PREPARE_SHUTDOWN.equals(anEvent.type())) {
				stopSync();
				if (null != getEnvironment()) {
					super.shutdown();
				}
				endPhase(PHASE_SHUTDOWN);
			} else if (EventTypes.SERVER_SHUTDOWN.equals(anEvent.type())) {
				stopSync();
				if (null != getEnvironment()) {
					super.terminate();
				}
				endPhase(PHASE_TERMINATE);
			} 
			// else {
			//	throw new javax.jbi.JBIException("JBIFW0049: Unknown event type (" + anEvent.type().type()
			//			+ ").");
			// }

			mLastEvent = anEvent.type();
		} catch (Throwable ex) {
			throw new IllegalStateException(ex);
		}
	}

	/**
	 * Performs one-time initialization of JBI system components during startup.
	 * Exceptions during component bootstrap are (a) unlikely and (b) not
	 * considered fatal, so any exceptions are just dumped to the server log to
	 * assist in debugging.
	 */
	private void bootstrapSystemComponents() {
		SystemComponentBootstrap bootstrap;

		try {
			bootstrap = new SystemComponentBootstrap(getEnvironment());
			bootstrap.configureHttpSoapDefaultPorts();
		} catch (Exception ex) {
			mLogger.log(Level.INFO,
					mTranslator.getString(LocalStringKeys.SYSTEM_COMPONENT_INIT_EXCEPTION), ex);
		}
	}

	/**
	 * Perform the ready() and sync() actions in a separate thread
	 */
	private void ready() {
		String asyncEnabled = System.getProperty("com.sun.jbi.AsyncReadySync", "true"); // Asynch
																						// is
																						// enabled
																						// by
																						// default
		if (Boolean.parseBoolean(asyncEnabled)) {
			mSyncThread = new Thread(this, "JBI-Ready-Sync");
			mSyncThread.setDaemon(true);
			mSyncThread.start();
		} else {
			run();
		}
	}

	/**
	 * Stop any ready/synchronization action that may be in process.
	 */
	private void stopSync() {
		if (mSyncThread != null) {
			if (mSyncProcessor != null) {
				mSyncProcessor.stop();
			}
			try {
				mSyncThread.join(60000);
			} catch (java.lang.InterruptedException iEx) {

			} finally {
				mSyncThread = null;
			}
		}
	}

	/**
	 * Have Framework bring the environment to ready, and then perform
	 * synchronization actions.
	 */
	public void run() {
		try {
			super.ready(true);
			{
				String syncEnabled = System.getProperty("com.sun.jbi.SyncEnabled", "true"); // synch
																							// is
																							// enabled
																							// by
																							// default
				if (Boolean.parseBoolean(syncEnabled) && mSyncProcessor != null) {
					mSyncProcessor.start();
				} else {
					mLogger.warning(mTranslator.getString(LocalStringKeys.JBI_SYNC_DISABLED));
				}
				mSyncProcessor = null;
			}
		} catch (Exception ex) {

		} finally {
			mSyncThread = null;
		}
	}

	/**
	 * Signal that particular SunJBIFramework phase has been completed.
	 */
	synchronized void endPhase(int phase) {
		mLogger.fine("JBI:endPhase-" + phase + ": " + (System.currentTimeMillis() - mStartStamp));
		mPhase = phase;
		this.notifyAll();
	}

	/**
	 * Wait for a particular SunJBIFramework phase to be completed.
	 */
	synchronized void waitForPhase(int phase) {
		while (phase > mPhase) {
			try {
				this.wait();
			} catch (InterruptedException iEx) {
			}
		}
		mLogger.fine("JBI:waitForPhase-" + phase + ": "
				+ (System.currentTimeMillis() - mStartStamp));
	}

	/**
	 * This method returns the startup time for the JBI Framework. It may be
	 * overridden for specific AS types. Sun AS provider includes the cost of
	 * synchronization.
	 * 
	 * @return long - time for startup
	 */
	@Override
	public long getStartupTime() {
		return (mStartTime + super.getStartupTime());
	}

	/**
     * The GF Lifecycle interface will call this method after START and before READY to
     * allow local XAResources to be declared for recovery.
     */
    public XAResource[] getXAResources()
    {
        //
        //  Just in case GF calls us while STARTUP is still active, we will wait until
        //  we know the parts of startup that interest us have completed.
        //
        mLogger.fine("JBI:getXAResources: " + (System.currentTimeMillis() - mStartStamp));

        waitForPhase(PHASE_STARTUP);

        //
        //  See if need to start the JBI framework for XA resource recovery resources
        //
        if (mSyncProcessor != null)
        {
            try
            {
                long startTime = System.currentTimeMillis();
                boolean isStartupActive = mRegistryHelper.isActiveStartup();
                mStartTime += System.currentTimeMillis() - startTime;
                setStartupTime();
                if (isStartupActive)
                {
                    mLogger.fine("JBI:XAActiveStartup");
                    super.prepare();
                    endPhase(PHASE_PREPARE);
                    // 0 = INIT_EVENT
                    mLastEvent = SERVER_INIT;
                }
            }
            catch (Exception e)
            {
                mLogger.warning(mTranslator.getString(LocalStringKeys.GET_XA_RESOURCES, e));
            }
        }
        
        //
        //  At this point we should have collected all local XAResources declared by JBI Components.
        //
        return (getEnvironment().getNormalizedMessageService().getXAResources());
    }

  //-------------------------ResourceRecoveryListener---------------------------------

    /**
     *  Callback stating the XAResource recovery has started.
     */
    public void recoveryStarted()
    {
        mLogger.fine("JBI:recoveryStarted: " + (System.currentTimeMillis() - mStartStamp));
        
    }
    
    /**
     *  Callback stating that XAResource recovery has completed.
     *  We purge an XA resource registrations held by the Message Service so as to 
     *  not hold on to any memory if the component is shutdown.
     */
    public void recoveryCompleted()
    {
        getEnvironment().getNormalizedMessageService().purgeXAResources();        
        mLogger.fine("JBI:recoveryCompleted: " + (System.currentTimeMillis() - mStartStamp));
    }
}
