/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SystemComponentBootstrap.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.glassfish3.jbi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import org.glassfish.internal.api.Globals;

import com.sun.enterprise.config.serverbeans.HttpService;
import com.sun.enterprise.config.serverbeans.HttpListener;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.StringTranslator;

/**
 * Bootstrap class for JBI components which come pre-installed with the JBI 
 * runtime (i.e. system components).  This class was created to 
 * handle one-time configuration and initialization requirements of system 
 * components.  Operations in this class should catch all but the most serious
 * exceptions locally instead of throwing them out to the caller.
 *
 * @author Sun Microsystems, Inc.
 */
public class SystemComponentBootstrap
{    
    /** Offset used to configure http and http/s port values in HTTP BC
     *  configuration file.
     */
    private static final int PORT_OFFSET = 1000;
    
    /** Relative path to the httpsoapbc configuration file. */
    private static final String HTTP_SOAP_WORKSPACE = 
            "components" + File.separator + "sun-http-binding" + File.separator + 
            "install_root" + File.separator + "workspace";
    
    /** Name of the httpsoapbc configuration file. */
    private static final String HTTP_SOAP_CONFIG = "config.properties";
    
    /** Names of the http and https listeners in domain.xml. */
    private static final String HTTP_LISTENER  = "http-listener-1";
    private static final String HTTPS_LISTENER = "http-listener-2";
    
    /** Token names used in httpsoapbc config file. */
    private static final String HTTP_TOKEN     = "${HttpDefaultPort}";
    private static final String HTTPS_TOKEN    = "${HttpsDefaultPort}";
    
    /** Default http and https port numbers. */
    private static final int HTTP_DEFAULT_PORT  = 9080;
    private static final int HTTPS_DEFAULT_PORT = 9181;
    
    private String              mConfigPath;
    private StringTranslator    mTranslator;
    private Logger              mLogger;
    private EnvironmentContext  mJbiCtx;

    /** Create a new instance of SystemComponentBootstrap.
     *  @param envCtx framework environment context
     *  @throws Exception when we fail while disovering appserver config
     */
    public SystemComponentBootstrap(EnvironmentContext envCtx)
        throws Exception
    {
        mJbiCtx         = envCtx;
        mTranslator     = envCtx.getStringTranslatorFor(this);
        mLogger         = Logger.getLogger("com.sun.jbi.framework.sun");
        
        mConfigPath     = envCtx.getJbiInstanceRoot() +
                File.separator + HTTP_SOAP_WORKSPACE + File.separator + HTTP_SOAP_CONFIG;
    }
    
    /** Populates the httpsoap BC's configuration file with default port values.
     *  This method will only perform token-replacement if required, so multiple
     *  calls are safe (basically a NOP).
     */
    public void configureHttpSoapDefaultPorts()
    {
        Properties props = readHttpSoapConfig();
        
        // make sure token substitution is necessary
        if (props.containsValue(HTTP_TOKEN) || props.containsValue(HTTPS_TOKEN))
        {
            setHttpSoapDefaultPorts(props);
        }        
    }
    
    /** Read the httpsoap BC config into a Properties object.
     *  @return Properties object containing httpsoap BC configuration.  This
     *  object will be empty if the config file was not found.
     */
    private Properties readHttpSoapConfig()
    {
        Properties      props = new Properties();
        FileInputStream fis = null;
        
        try
        {
            fis = new FileInputStream(mConfigPath);
            props.load(fis);
        }
        catch (java.io.IOException ioEx)
        {
            mLogger.fine(mTranslator.getString(
                    LocalStringKeys.HTTP_SOAP_CONFIG_READ_ERROR, 
                    mConfigPath, ioEx.getMessage()));
        }
        finally
        {
            if (fis != null)
            {
                try
                {
                    fis.close();
                }
                catch (java.io.IOException ioEx) {}
            }
        }
        
        return props;                
    }
    
    /** Replaces tokens in the httpsoap BC configuration file and writes a new
     *  copy into the component's workspace.
     *  @param props Properties object containing config values to be serialized
     *  into configuration file.
     */
    private void setHttpSoapDefaultPorts(Properties props)
    {
        HttpService     httpService;
        HttpListener    httpListener;
        HttpListener    httpsListener;
        int httpPort;
        int httpsPort;
        
        // query appserver config for default http/https port values
        try
        {
        	httpService = Globals.getDefaultHabitat().getComponent(HttpService.class);
            httpListener    = httpService.getHttpListenerById(HTTP_LISTENER);
            httpsListener   = httpService.getHttpListenerById(HTTPS_LISTENER);
        }
        catch (Exception ex)
        {
            mLogger.warning(mTranslator.getString(
                    LocalStringKeys.APPSERVER_CONFIG_ERROR, 
                    mConfigPath, ex.getMessage()));
            
            return;
        }

        // If HTTP listeners were found for GlassFish, just bump the port
        // values by 1000 for the HTTP BC. Otherwise, use reasonable default
        // port numbers. If ports are in use, bump the port number until an
        // available port is found.
        if ( null != httpListener )
        {
            httpPort = Integer.parseInt(httpListener.getPort()) + PORT_OFFSET;
        }
        else
        {
            httpPort = HTTP_DEFAULT_PORT;
        }
        while ( portInUse(httpPort) )
        {
           ++httpPort;
        }
        if ( null != httpsListener )
        {
            httpsPort = Integer.parseInt(httpsListener.getPort()) + PORT_OFFSET;
        }
        else
        {
            httpsPort = HTTPS_DEFAULT_PORT;
        }
        while ( portInUse(httpsPort) )
        {
           ++httpsPort;
           if ( httpsPort == httpPort )
           {
               ++httpsPort;
           }
        }

        // search and replace in the properties object
        for (Map.Entry entry : props.entrySet())
        {
            if (entry.getValue().equals(HTTP_TOKEN))
            {
                entry.setValue(String.valueOf(httpPort));
            }
            else if (entry.getValue().equals(HTTPS_TOKEN))
            {                
                entry.setValue(String.valueOf(httpsPort));
            }
        }
        
        // write the new properties file
        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(mConfigPath);
            props.store(fos, null);
        }
        catch (java.io.IOException ioEx)
        {
            mLogger.warning(mTranslator.getString(
                    LocalStringKeys.HTTP_SOAP_CONFIG_WRITE_ERROR, 
                    mConfigPath, ioEx.getMessage()));
        }
        finally
        {
            if (fos != null)
            {
                try
                {
                    fos.close();
                }
                catch (java.io.IOException ioEx) {}
            }
        }

    }
    
    /** Checks to see if a port is in use. If so, returns true, otherwise
     *  returns false. This method works by attempting to open a socket 
     *  connection to the port - if that succeeds, then the port is in use.
     *  @param port The port number to check
     */
    private boolean portInUse(int port)
    {
        boolean ret = false;
        try
        {
            Socket s = new Socket("127.0.0.1", port);
            ret = true;
            s.close();
        }
        catch (Exception e)
        {
        }
        return ret;
    }

}
