package org.openesb.glassfish3.jbi;

import javax.management.MBeanServerConnection;

/**
 * The JBI Runtime needs information on the servers/clusters in a domain, state of an
 * instance etc. This is the interface to get this application server context information.
 *
 * @author Sun Microsystems, Inc.
 */
public interface AppServerContext {
    /*----------------------------------------------------------------------------------*\
     *         Common operations invoked from the DAS and non-DAS instances.            *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * Determine if this instance is the central administration server.
     *
     * @return true if this instance is the central administration server.
     */
    boolean isDAS();
    
    /**
     * Get the "target" for this instance. If this instance :
     * <ul>
     *   <li> is the central admininistration server instance then target = "server" </li>
     *   <li> is a clustered instance then target = [cluster-name] </li>
     *   <li> is a standalone instance then target = [server-name] </li>
     * </ul>
     * <br/>
     * This is equivalent to calling getTargetName(getInstanceName()).
     *
     * @return the "target" for this instance.
     */
    String getTargetName()
        throws Exception;
    
    /**
     * Get the MBeanServerConnection for a specific instance.
     *
     * @param instanceName - instance name, the MBeanServerConnection for which is to 
     *                       be obtained.
     * @return the MBeanServerConnection to a given instance.
     * @throws Exception on errors
     */
    MBeanServerConnection   getMBeanServerConnection(String instanceName)
        throws Exception;
    
    /**
     * Get the name of the server instance. In case this is the central administartion 
     * server then instance name = "server".
     *
     * @return the name of the glassfish instance.
     */
    String getInstanceName();
    
    /*----------------------------------------------------------------------------------*\
     *         Operations invoked from the DAS only.                                    *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * Determine the runtime state of an instance.
     *
     * @param instanceName - name of the instance whose state is to be determined.
     * @return true if the specified instance is running false is it is shutdown
     */
    boolean isInstanceUp(String instanceName);
    
    /**
     * Determine if the central administraion server supports multiple servers.
     *
     * @return true if this administration server supports multiple servers.
     */
    boolean multipleServersSupported();
    
    /**
     * Get the names of all the non-clustered standalone servers in the domain.
     *
     * @return an array of names of all the standalone servers in the domain.
     */
    String[] getStandaloneServerNames();
    
    /**
     * Get the names of all the clsuters in the domain.
     *
     * @return an array of names of all the clusters in the domain. If the domain has zero
     * clusters then an empty array is returned.
     */
    String[] getClusterNames();
    
    /**
     * Get the names of all the member servers in the specified cluster.
     *
     * @return an array of names of all the servers in the cluster. If the clusterName is
     * a non-existent cluster or does not have any member instances then an empty 
     * array is returned
     */ 
    String[] getServersInCluster(String clusterName);
    
    /**
     * Get the "target" for the specified instance.
     *
     * <ul>
     *   <li> is the central admininistration server instance then target = "server" </li>
     *   <li> is a clustered instance then target = [cluster-name] </li>
     *   <li> is a standalone instance then target = [server-name] </li>
     * </ul>
     *
     * @param instanceName - name of the instance, whose target is to be determined
     * @return the "target" for the specific instance
     */
    String getTargetName(String instanceName)
        throws Exception;
    /**
     *
     * @return true if the instance is clustered
     * @param instanceName - instance name
     */
    boolean isInstanceClustered(String instanceName)
        throws Exception;
}
