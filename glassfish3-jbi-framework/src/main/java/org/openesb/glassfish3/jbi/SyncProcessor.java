/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SyncProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SyncProcessor.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 2, 2006, 4:49 PM
 */

package org.openesb.glassfish3.jbi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.jbi.management.DeploymentServiceMBean;
import javax.jbi.management.LifeCycleMBean;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeType;

import org.glassfish.internal.api.Globals;

import com.sun.enterprise.config.serverbeans.HttpListener;
import com.sun.enterprise.config.serverbeans.HttpService;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceAssemblyState;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.framework.EnvironmentContext;
import com.sun.jbi.management.ComponentInfo.Variable;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.config.ConfigurationBuilder;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.registry.Updater;
import com.sun.jbi.management.registry.data.ComponentInfoImpl;
import com.sun.jbi.management.system.DeploymentService;
import com.sun.jbi.management.system.InstallationService;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.util.ComponentConfigurationHelper;



/**
 * This class controls the synchronization of JBI registry from the DAS to this running instance.
 * This instance can either be a standalone instance or a cluster member instance.
 * The basic flow is:
 *      Get current registry from the DAS.
 *      Diff the current registry with the registry contained in the instance.
 *      Compute the actions that are needed to reconcile the differences.
 *      Execute the actions
 */
public class SyncProcessor
{
    private static final String	STRING_TRANSLATOR_NAME = "com.sun.jbi.framework.sun";

    private RegistryHelper                  mRegistryHelper;
    private ManagementContext               mMgrCtx;
    private EnvironmentContext              mEnvCtx;
    private Registry                        mRegistry;
    private Updater                         mUpdater;
    private Logger                          mLogger;
    private StringTranslator                mTranslator;
    private InstallationService             mIS;
    private DeploymentService               mDS;
    private MBeanServer                     mMBeanServer;
    private MBeanServerConnection           mDASMBeanServer;
    private Map<String, ObjectName>         mCompLifeCycle;
    private ComponentConfigurationHelper    mConfigHelper;
    private volatile boolean              mRunning;
    
    /**
     * Constructor for new instance of the SyncHelper.
     * @param envCtx the EnvironmentContext.
     * @param registryHelper the registry helper for accessing the registry.
     */
    public SyncProcessor(EnvironmentContext envCtx, RegistryHelper registryHelper)
    {
        mEnvCtx = envCtx;
        mMgrCtx = ((com.sun.jbi.management.system.ManagementService)envCtx.getManagementService()).getManagementContext();
        mTranslator = mEnvCtx.getStringTranslator(STRING_TRANSLATOR_NAME);
        mLogger = Logger.getLogger("com.sun.jbi.framework.sun");  
        mIS = mMgrCtx.getInstallationServiceHandle();
        mDS = mMgrCtx.getDeploymentServiceHandle();
        mMBeanServer = envCtx.getMBeanServer();
        mCompLifeCycle = new HashMap();
        mRegistryHelper = registryHelper;
        mConfigHelper = new ComponentConfigurationHelper();
    }
    
    /**
     * Start sync processing.
     */
    public void start()
    {        
        if (mRegistryHelper.isSyncRequired())
        {
            try
            {
                mLogger.fine("JBI-Synchronization started.");
                mRunning = true;
                mRegistry = (com.sun.jbi.management.registry.Registry)mEnvCtx.getRegistry();
                mUpdater = mRegistry.getUpdater();
                if (mRegistryHelper.diffRegistry())
                {
                    mRegistryHelper.displayActions();
                    if (mRunning)
                    {
                        applyRuntimeConfiguration();
                    }
                    if (mRunning)
                    {
                        removeComponentConfig();
                    }
                    if (mRunning)
                    {
                        startComponentsForUndeploy();
                    }
                    if (mRunning)
                    {
                        undeployOldServiceAssemblies();
                    }
                    if (mRunning)
                    {
                        undeployAffectedServiceAssemblies();
                    }
                    if (mRunning)
                    {
                        shutdownOldComponents();
                    }
                    if (mRunning)
                    {
                        shutdownReplacedComponents();
                    }
                    if (mRunning)
                    {
                        shutdownAffectedComponents();
                    }
                    if (mRunning)
                    {
                        shutdownAllComponents();
                    }
                    if (mRunning)
                    {
                        uninstallOldComponents();
                    }
                    if (mRunning)
                    {
                        uninstallReplacedComponents();
                    }
                    if (mRunning)
                    {
                        uninstallOldSharedLibraries();
                    }
                    if (mRunning)
                    {
                        uninstallReplacedSharedLibraries();
                    }
                    if (mRunning)
                    {
                        installNewSharedLibraries();
                    }
                    if (mRunning)
                    {
                        installReplacedSharedLibraries();
                    }
                    if (mRunning)
                    {
                        updateComponents();
                    }
                    if (mRunning)
                    {
                        installNewComponents();
                    }
                    if (mRunning)
                    {
                        installReplacedComponents();
                    }
                    if (mRunning)
                    {
                        startComponentsForConfigChanges();
                    }
                    if (mRunning)
                    {
                        applyRuntimeConfiguration();
                        removeComponentAppVars();
                        addComponentAppVars();
                        removeComponentConfig();
                        addComponentConfig();
                        applyComponentProperties();
                    }
                    if (mRunning)
                    {
                        startComponentsForDeploy();
                    }
                    if (mRunning)
                    {
                        deployNewServiceAssemblies();
                    }  
                    if (mRunning)
                    {
                        deployReplacedServiceAssemblies();
                    }  
                    if (mRunning)
                    {
                        configureComponentLifeCycle();
                    }
                    if (mRunning)
                    {
                        configureServiceAssemblyLifeCycle();
                    }
                }
            }
            catch (Exception ex)
            {
                mLogger.severe(mTranslator.getString(LocalStringKeys.JBI_SYNC_FAILED, ex));         
            }
            finally
            {
                if (mRegistryHelper != null)
                {
                    mRegistryHelper.cleanup();
                }
                mRunning = false;
            }
            mLogger.fine("JBI-Synchronization completed.");
        }
    }
    
    public void stop()
    {
        mLogger.fine("JBI-Synchronization stopped.");
        mRunning = false;
    }
        
//---------------------- Shared Library Operations ---------------------------------
    
    /**
     * Download and install any new shared libraries.
     * @throws SyncException if anything bad happens.
     */
    private void installNewSharedLibraries()
        throws SyncException
    {
        installSharedLibraries(mRegistryHelper.getNewSharedLibraries());
    }
    
    /**
     * Download and install any replaced shared libraries.
     * @throws SyncException if anything bad happens.
     */
    private void installReplacedSharedLibraries()
        throws SyncException
    {
        installSharedLibraries(mRegistryHelper.getReplacedSharedLibraries());
    }
    
    /**
     * Download and install any new shared libraries.
     * @throws SyncException if anything bad happens.
     */
    private void installSharedLibraries(List<String> libraries)
        throws SyncException
    {
        for (String slName : libraries)
        {
            String      slFileName = "file://" + mRegistryHelper.downloadSharedLibraryArchive(slName);
            String      newSlName;    
            
            newSlName = mIS.installSharedLibrary(slFileName);
        }
    }
    
    /**
     * Uninstall any old shared libraries.
     * @throws SyncException if anything bad happens.
     */
    private void uninstallOldSharedLibraries()
        throws SyncException
    {
        uninstallSharedLibraries(mRegistryHelper.getOldSharedLibraries());
    }

    /**
     * Uninstall any replaced shared libraries.
     * @throws SyncException if anything bad happens.
     */
    private void uninstallReplacedSharedLibraries()
        throws SyncException
    {
        uninstallSharedLibraries(mRegistryHelper.getReplacedSharedLibraries());
    }

    /**
     * Uninstall any old shared libraries.
     * @throws SyncException if anything bad happens.
     */
    private void uninstallSharedLibraries(List<String> libraries)
        throws SyncException
    {
        for (String slName : libraries)
        {
            try
            {
                mIS.uninstallSharedLibrary(slName);
            }
            catch (Exception e)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_UNINSTALL_SL,
                    slName, e));
                
            }
        }
    }
   
//---------------------- Component Operations ---------------------------------
    
    /**
     * Download and install any new components.
     * @throws SyncException if anything bad happens.
     */
    private void installNewComponents()
        throws SyncException
    {
        installComponents(mRegistryHelper.getNewComponents());
    }
         
    /**
     * Download and install any replaced components.
     * @throws SyncException if anything bad happens.
     */
    private void installReplacedComponents()
        throws SyncException
    {
        installComponents(mRegistryHelper.getReplacedComponents());
    }
    
    /**
     * Download and install any new components.
     * @throws SyncException if anything bad happens.
     */
    private void installComponents(List<String> components)
        throws SyncException
    {
        for (String compName : components)
        {
            try
            {
                String              compFileName = "file://" + mRegistryHelper.downloadComponentArchive(compName);
                ComponentInfoImpl   comp = (ComponentInfoImpl)mRegistryHelper.getNewComponentInfo(compName);
                ObjectName          compInstaller;
                ObjectName          compLifecycle;

                compInstaller = mIS.loadNewInstaller(compFileName);
                configureComponent(compInstaller, compName);
                compLifecycle = installComponent(compInstaller);
                propertyHack(compName);
                mIS.unloadInstaller(compName, false);
                if (comp.getUpgradeNumber() != 0)
                {
                     mIS.upgradeComponent(compName, comp.getUpgradeNumber());
                }
                mCompLifeCycle.put(compName, compLifecycle);
            }
            catch (Exception ex)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_INSTALL_COMP,
                        compName, ex));                
            }
        }
    }
   
    /*
     * Install a component.
     * @param installer - the ObjectName of the installer MBean
     * @return ObjectName of the ComponentLifeCycleMBean
     * @throws SyncException if anything bad happens
     */
    private ObjectName installComponent(ObjectName installer)
        throws SyncException
    {
        ObjectName  compLifecycle = null;
        ObjectName  configuration = null;
        
        Exception   e;
        
        try
        {            
              compLifecycle = (ObjectName)mMBeanServer.invoke(installer, "install", null, null);
              return (compLifecycle);
        }
        catch (javax.management.InstanceNotFoundException infEx)
        {
            e = infEx;
        }
        catch (javax.management.MBeanException mbEx)
        {
            e = mbEx;
        }
        catch (javax.management.ReflectionException rEx)
        {
            e = rEx;            
        }
        throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_INSTALL_COMP,
                compLifecycle, e));
    }
    
    /**
     * Download and install any updated components.
     * @throws SyncException if anything bad happens.
     */
    private void updateComponents()
        throws SyncException
    {
        for (String compName : mRegistryHelper.getUpdatedComponents())
        {
            try
            {
               String               compFileName = "file://" + mRegistryHelper.downloadComponentArchive(compName);
               ComponentInfoImpl    comp = (ComponentInfoImpl)mRegistryHelper.getNewComponentInfo(compName);
               String               status;
               
                  status = mIS.upgradeComponent(compName, compFileName);
                  mIS.upgradeComponent(compName, comp.getUpgradeNumber());
            }
            catch (Exception ex)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_INSTALL_COMP,
                        compName, ex));                
            }
        }
    }
    
    /**
     * Uninstall any old components.
     * @throws SyncException if anything bad happens.
     */
    private void uninstallOldComponents()
        throws SyncException
    {
        uninstallComponents(mRegistryHelper.getOldComponents());
    }
    
    /**
     * Uninstall any replaced components.
     * @throws SyncException if anything bad happens.
     */
    private void uninstallReplacedComponents()
        throws SyncException
    {
        uninstallComponents(mRegistryHelper.getReplacedComponents());
    }
    
    /**
     * Uninstall  components.
     * @throws SyncException if anything bad happens.
     */
    private void uninstallComponents(List<String> components)
        throws SyncException
    {
        for (String compName : components)
        {
            try
            {
                ObjectName  compInstaller;
                ObjectName  compLifecycle;

                compInstaller = mIS.loadInstaller(compName);
                uninstallComponent(compInstaller);
                mIS.unloadInstaller(compName, true);
            }
            catch (Exception ex)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_UNINSTALL_COMP,
                    compName, ex));               
            }
         }
    }
    
    /*
     * Uninstall a component.
     * @param installer - the ObjectName of the installer MBean
     * @throws SyncException if anything bad happens
     */
    private void uninstallComponent(ObjectName installer)
            throws SyncException
    {
        Exception   e;
        
        try
        {
              mMBeanServer.invoke(installer, "uninstall", null, null);
              return;
        }
        catch (javax.management.InstanceNotFoundException infEx)
        {
            e = infEx;
        }
        catch (javax.management.MBeanException mbEx)
        {
            e = mbEx;
        }
        catch (javax.management.ReflectionException rEx)
        {
            e = rEx;
        }
        throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_UNINSTALL_COMP,
                installer, e));
    }
    
    /*
     * Get the current state of a Component
     * @param installer - the ObjectName of the ComponentLifeCycleMBean
     * @return String containing the state
     * @throws SyncException if anything bad happens
     */
    private String getComponentState(ObjectName compName)
        throws SyncException
    {
        Exception   e;
        
        try
        {
              return ((String)mMBeanServer.getAttribute(
                      compName, "CurrentState"));
        }
        catch (javax.management.InstanceNotFoundException infEx)
        {
            e = infEx;
        }
        catch (javax.management.MBeanException mbEx)
        {
            e = mbEx;
        }
        catch (javax.management.ReflectionException rEx)
        {
            e = rEx;
        }        
        catch (javax.management.AttributeNotFoundException aEx)
        {
            e = aEx;
        }
        throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_GET_COMP_STATE,
                compName, e));
    }

     /*
     * Set the current state of a Component
     * @param installer - the ObjectName of the ComponentLifeCycleMBean
     * @param sate - String containing the state
     * @throws SyncException if anything bad happens
     */
   private void setComponentState(ObjectName compName, String state)
        throws SyncException
    {
        Exception       e;
        String          operation;
        
        if (state.equals(LifeCycleMBean.STARTED))
        {
            operation = "start";
        }
        else if (state.equals(LifeCycleMBean.SHUTDOWN))
        {
            operation = "shutDown";
        }
        else if (state.equals(LifeCycleMBean.STOPPED))
        {
            operation = "stop";
        }
        else
        {
            return;
        }
        
        try
        {
              mMBeanServer.invoke(compName, operation, null, null);
              return;
        }
        catch (javax.management.InstanceNotFoundException infEx)
        {
            e = infEx;            
        }
        catch (javax.management.MBeanException mbEx)
        {
            e = mbEx;
        }
        catch (javax.management.ReflectionException rEx)
        {
            e = rEx;
        }
        throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_SET_COMP_STATE,
                compName, e));
    }
    
    /**
    * Configure the component MBean.
    * @param installer - the ObjectName of the InstallerMBean
    * @param sate - String containing the name of the Component
    * @throws SyncException if anything bad happens
    */
   private void configureComponent(ObjectName installer, String compName)
        throws SyncException
    {
        List<String>    comps;
        ComponentInfo   comp = mRegistryHelper.getNewComponentInfo(compName);
        Map             props = comp.getProperties();
        ObjectName      configuration;
        Exception       e;
        
        if (!props.isEmpty())
        {
            AttributeList   al = new AttributeList();
            for (Object prop : props.entrySet() )
            {
                al.add(new Attribute((String)((Map.Entry)prop).getKey(), ((Map.Entry)prop).getValue()));
            }
            
            try
            {
                configuration = (ObjectName)mMBeanServer.invoke(installer, "getInstallerConfigurationMBean", 
                    null, null);
                if (configuration != null)
                {
                    mMBeanServer.setAttributes(configuration, al);
                }
                return;
            }
            catch (javax.management.InstanceNotFoundException infEx)
            {
                e = infEx;            
            }
            catch (javax.management.MBeanException mbEx)
            {
                e = mbEx;
            }
            catch (javax.management.ReflectionException rEx)
            {
                e = rEx;
            }
            throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_SET_COMP_CONFIGURATION,
                    compName, e));
        }
    }
   
  /**
    * Set all components to their desired state.
    * @throws SyncException if anything bad happens
    */
   private void configureComponentLifeCycle()
        throws SyncException
    {
        List<String>    comps;
        
        try
        {
            comps = mRegistry.getComponentQuery().getComponentIds(ComponentType.BINDINGS_AND_ENGINES);            
        } 
        catch (RegistryException rEx)
        {
            throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_FAILED,
                rEx));           
        }
        
        for (String comp : comps)
        {
            String      desired = ComponentState.getLifeCycleState(mRegistryHelper.getNewComponentInfo(comp).getStatus());
            String      actual = getComponentState(getComponentLifeCycle(comp));
            
            if (!desired.equals(actual))
            {
                setComponentState(getComponentLifeCycle(comp), desired);                
            }
        }      
    }

    //---------------------- Service Assembly Operations ---------------------------------

    /**
     * Download and deploy any new service assemblies.
     * @throws SyncException if anything bad happens.
     */
    private void deployNewServiceAssemblies()
        throws SyncException
    {
        for (String saName : mRegistryHelper.getNewServiceAssemblies())
        {
            String        saFileName = "file://" + mRegistryHelper.downloadServiceAssemblyArchive(saName);

            try
            {
                mDS.deploy(saFileName);
            }
            catch (Exception ex)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_DEPLOY_SA,
                    saName, ex));               
            }
        }
    }
    
    /**
     * Download and deploy any new service assemblies.
     * @throws SyncException if anything bad happens.
     */
    private void deployReplacedServiceAssemblies()
        throws SyncException
    {
        for (String saName : mRegistryHelper.getReplacedServiceAssemblies())
        {
            String        saFileName = "file://" + mRegistryHelper.downloadServiceAssemblyArchive(saName);

            try
            {
                mDS.deploy(saFileName);
            }
            catch (Exception ex)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_DEPLOY_SA,
                    saName, ex));               
            }
        }
    }
    
    /**
     * Undeploy any old service assemblies.
     * @throws SyncException if anything bad happens.
     */
    private void undeployOldServiceAssemblies()
        throws SyncException
    {
        for (String saName : mRegistryHelper.getOldServiceAssemblies())
        {
            try
            {
                if (mDS.getState(saName).equals(ServiceAssemblyState.STARTED.toString()))
                {
                    mDS.stop(saName);
                }
                if (mDS.getState(saName).equals(ServiceAssemblyState.STOPPED.toString()))
                {
                    mDS.shutDown(saName);
                }
                mDS.undeploy(saName);
            }
            catch (Exception ex)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_UNDEPLOY_SA,
                    saName, ex));                               
            }
        }
    }

    /**
     * Undeploy any service assemblies effect by other changes.
     * @throws SyncException if anything bad happens.
     */
    private void undeployAffectedServiceAssemblies()
        throws SyncException
    {
        for (String saName : mRegistryHelper.getAffectedServiceAssemblies())
        {
            try
            {
                if (mDS.getState(saName).equals(ServiceAssemblyState.STARTED.toString()))
                {
                    mDS.stop(saName);
                }
                if (mDS.getState(saName).equals(ServiceAssemblyState.STOPPED.toString()))
                {
                    mDS.shutDown(saName);
                }
                mDS.undeploy(saName);
            }
            catch (Exception ex)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_UNDEPLOY_SA,
                    saName, ex));                               
            }
        }
    }

    /**
     * Get the current state of a service assembly.
     * @return String containing the state
     * @throws SyncException if anything bad happens.
     */
    private String getServiceAssemblyState(String saName)
        throws SyncException
    {
        try
        {
            return (mDS.getState(saName));
        }
        catch (Exception ex)
        {
            throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_GET_SA_STATE,
                saName, ex));                               
        }
    }

    /**
     * Set the current state of a service assembly.
     * @param saName - name of the service assembly
     * @param state - the new state
     * @throws SyncException if anything bad happens.
     */
    private void setServiceAssemblyState(String saName, String state)
        throws SyncException
    {
        try
        {
            if (state.equals(DeploymentServiceMBean.STARTED))
            {
                mDS.start(saName);
            }
            else if (state.equals(DeploymentServiceMBean.SHUTDOWN))
            {
                mDS.shutDown(saName);
            }
            else if (state.equals(DeploymentServiceMBean.STOPPED))
            {
                mDS.stop(saName);
            }
        }
        catch (Exception ex)
        {
            throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_SET_SA_STATE,
                saName, state, ex));                               
        }
    }
    
   /**
    * Set all service assemblies to their desired state.
    * @throws SyncException if anything bad happens
    */
    private void configureServiceAssemblyLifeCycle()
        throws SyncException
    {
       List<String>    sas;
        
        try
        {
            sas = mRegistry.getServiceAssemblyQuery().getServiceAssemblies();            
        } 
        catch (RegistryException rEx)
        {
            throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_FAILED,
                rEx));           
        }
        for (String saName : sas)
        {
            String    desired = ServiceAssemblyState.convertState(mRegistryHelper.getNewServiceAssemblyState(saName));
            String    actual = getServiceAssemblyState(saName);
            
            if (!desired.equals(actual))
            {
                setServiceAssemblyState(saName, desired);
            }
        }
    }

    //---------------------- Runtime Configuration Changes ---------------------------

    /**
     * Apply any changes to the runtime configuration.
     */
    private void applyRuntimeConfiguration()
        throws SyncException
    {
        applyGlobalConfiguration(ConfigurationCategory.System);
        applyGlobalConfiguration(ConfigurationCategory.Logger);
        applyGlobalConfiguration(ConfigurationCategory.Deployment);
        applyGlobalConfiguration(ConfigurationCategory.Installation);
        applyConfiguration(ConfigurationCategory.System);
        applyConfiguration(ConfigurationCategory.Logger);
        applyConfiguration(ConfigurationCategory.Deployment);
        applyConfiguration(ConfigurationCategory.Installation);
   }

    private void applyGlobalConfiguration(ConfigurationCategory category)
        throws SyncException
    {
        Map<String,String>  map = mRegistryHelper.getGlobalConfigChanges(category.toString());
        if (map != null)
        {
            apply("domain", category, map);
        }
    }
    
    private void applyConfiguration(ConfigurationCategory category)
        throws SyncException
    {
        Map<String,String>  map = mRegistryHelper.getConfigChanges(category.toString());
        if (map != null)
        {
            apply(mEnvCtx.getPlatformContext().getTargetName(), category, map);
        }
    }
    
    private void apply(String target, ConfigurationCategory category, Map<String, String> map)
        throws SyncException
    {
        AttributeList   al = new AttributeList();
        
        for (Map.Entry<String,String> e : map.entrySet())
        {
            if (e.getValue() != null)
            {
                al.add(new Attribute(e.getKey(), e.getValue()));
            }
        }
        if (al.size() == 0)
        {
            return;
        }
        try
        {
            MBeanNames.ServiceType st = MBeanNames.ServiceType.valueOf(category.toString());
            ObjectName mb = new ObjectName("com.sun.jbi:JbiName=" + target + "," +
                    "ServiceName=ConfigurationService,ControlType=" + ConfigurationBuilder.getControlType(st) +
                    ",ComponentType=System");
            mMBeanServer.setAttributes(mb, al);
        }
        catch (Exception e)
        {
            throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_APPLY_CONFIGURATION, category.name(), target, e));
        }
    }
    
    //---------------------- Component  Configuration Changes ---------------------------

    private void applyComponentProperties()
        throws SyncException
    {
        Map<String, Properties>    compProps = mRegistryHelper.getComponentPropertyUpdates();
        
        for (String i : compProps.keySet())
        {
            Properties          props = compProps.get(i);
            ObjectName          on = getComponentConfigMBeanName(i);
            String              propName = "";
             
            try
            {
                MBeanInfo           mbi = mMBeanServer.getMBeanInfo(on);
                MBeanAttributeInfo  mbai[] = mbi.getAttributes();
                
                for (Object prop : props.keySet())
                {
                    String              typeName = null;
                    Object              value = null;

                    propName = (String)prop;
                    for (int j = 0 ; j < mbai.length; j++)
                    {
                        if (mbai[j].getName().equals(propName))
                        {
                            typeName = mbai[j].getType();
                            break;
                        }
                    }
                    mLogger.info("Apply " + i + " " + prop + " " + typeName +  "\n");
                    if (typeName != null)
                    {
                        Class objClass = Class.forName(typeName);
                        Class[] argClass = new Class[] {String.class};
                        java.lang.reflect.Constructor objConstructor = objClass.getConstructor(argClass);
                        String[] argValue = { props.getProperty(propName) };
                        value = (Object)objConstructor.newInstance(argValue);
                    }
                    mMBeanServer.setAttribute(on, new Attribute(propName, value));
                }
            }
            catch (Exception e)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_SET_COMP_PROP, propName, i, e));
            }
        }       
    }
    
    private void removeComponentAppVars()
        throws SyncException
    {
        Map <String, String[]>    appVars = mRegistryHelper.getRemoveComponentAppVars();
        
        for (String compName : appVars.keySet())
        {
            String      varName = "";
            
            try
            {
                ObjectName  on = getComponentConfigMBeanName(compName);
                String[]    vars = appVars.get(compName);
                
                for (int i = 0; i < vars.length; i++)
                {
                    Object[] params = new Object[]{vars[i]};
                    String[] sign = new String[]{"java.lang.String"};
                    varName = vars[i];
                    mLogger.fine("Delete AppVar " + compName + " " + (String)vars[i] + "\n");
                    mMBeanServer.invoke(on, "deleteApplicationVariable", params, sign);
                }
            }
            catch (Exception e)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_REMOVE_APP_VAR, varName, compName, e));
            }
        }        
    }
    
    private void addComponentAppVars()
        throws SyncException
    {
        Map <String, Variable[]>    appVars = mRegistryHelper.getAddComponentAppVars();
        
        for (String compName : appVars.keySet())
        {
            String  varName = "";
            
            try
            {
                ObjectName  on = getComponentConfigMBeanName(compName);
                Variable[]    vars = appVars.get(compName);
                
                for (int i = 0; i < vars.length; i++)
                {
                    String[] sign = new String[]
                        { "java.lang.String", "javax.management.openmbean.CompositeData" };
                    Object[] params = new Object[] 
                        { vars[i].getName(), 
                            mConfigHelper.createApplicationVariableComposite(
                                 vars[i].getName(), vars[i].getValue(), vars[i].getType()) };                
                    varName = vars[i].getName();
                    mLogger.fine("Add AppVar " + compName + " " + vars[i].getName() + "\n");
                    try
                    {
                        mMBeanServer.invoke(on, "addApplicationVariable", params, sign);
                    }
                    catch (javax.management.RuntimeMBeanException rEx)
                    {
                        if (rEx.getCause() instanceof javax.management.openmbean.KeyAlreadyExistsException)
                        {
                            mLogger.fine("Add(Set-Retry) Config " + compName + "\n");
                            mMBeanServer.invoke(on, "setApplicationVariable", params, sign);                      
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_ADD_APP_VAR, varName, compName, e));
            }
        }                
    }
    
    private void removeComponentConfig()
        throws SyncException
    {
        Map <String, String>    compConfigs = mRegistryHelper.getRemoveComponentConfigs();
        
        for (String compName : compConfigs.keySet())
        {
            String      config = compConfigs.get(compName);
           
            try
            {
                ObjectName  on = getComponentConfigMBeanName(compName);
                Object[]    params = new Object[]{config};
                String[]    sign = new String[]{"java.lang.String"};

                mLogger.fine("Remove Config " + compName + " " + config + "\n");
                mMBeanServer.invoke(on, "deleteApplicationConfiguration", params, sign);
            }
            catch (Exception e)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_REMOVE_APP_CONFIG, config, compName, e));
            }
        }       
    }
    
    private void addComponentConfig()
        throws SyncException
    {
        Map <String, Map<String, Properties>>    compConfigs = mRegistryHelper.getAddComponentConfigs();
        
        for (String compName : compConfigs.keySet())
        {
            String      configName = "";
            
            try
            {
                ObjectName      on = getComponentConfigMBeanName(compName);
                CompositeType   configType;
                {
                    String[] sign = new String[] {};
                    Object[] params = new Object[] {};
                    Object   o;
                    configType = (CompositeType)mMBeanServer.invoke(on, "queryApplicationConfigurationType", params, sign);
                }
                {
                    for (String config : compConfigs.get(compName).keySet())
                    {
                        Properties  props = compConfigs.get(compName).get(config);
                        String[] sign = new String[]
                            { "java.lang.String", "javax.management.openmbean.CompositeData" };
                        Object[] params = new Object[] 
                            { config, 
                              mConfigHelper.convertPropertiesToCompositeData(props, configType) };
                        mLogger.fine("Add Config " + compName + " " + config + "\n");
                        configName = config;
                        mMBeanServer.invoke(on, "addApplicationConfiguration", params, sign);
                    }
                }
            }
            catch (Exception e)
            {
                throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_ADD_APP_CONFIG, configName, compName, e));
            }
        }       
    }
    
    private ObjectName getComponentConfigMBeanName(String compName)
    {
        ComponentInfo       comp;
        MBeanNames          mbeanNames = mMgrCtx.getMBeanNames();
        ObjectName          cfgMBeanName = null;

        try
        {
            comp = mRegistry.getComponentQuery().getComponentInfo(compName);
            if ( comp.getComponentType() == ComponentType.BINDING )
            {
                cfgMBeanName = mbeanNames.getBindingMBeanName(compName, 
                    com.sun.jbi.management.MBeanNames.CONTROL_TYPE_CONFIGURATION);
            }
            else
            {
                cfgMBeanName = mbeanNames.getEngineMBeanName(compName, 
                    com.sun.jbi.management.MBeanNames.CONTROL_TYPE_CONFIGURATION);
            }
        }
        catch (com.sun.jbi.management.registry.RegistryException rEx)
        {
            
        }
        return cfgMBeanName;
    }

    //------------------------------- Helpers ----------------------------------------

    /**
     * Start any components needed for deploy of new service assemblies.
     * @throws SyncException if anything bad happens
     */
    private void startComponentsForDeploy()
        throws SyncException
    {
        List<String>    comps = mRegistryHelper.componentsToStartForDeploy();
        
        for (String comp : comps)
        {
            setComponentState(getComponentLifeCycle(comp), LifeCycleMBean.STARTED);
        }
    }
    
    /**
     * Start any components needed for undeploy of old service assemblies.
     * @throws SyncException if anything bad happens
     */
    private void startComponentsForUndeploy()
        throws SyncException
    {
        List<String>    comps = mRegistryHelper.componentsToStartForUndeploy();
        
        for (String comp : comps)
        {
            setComponentState(getComponentLifeCycle(comp), LifeCycleMBean.STARTED);
        }
    }
    
    /**
     * Start any components needed for component configuration changes.
     * @throws SyncException if anything bad happens
     */
    private void startComponentsForConfigChanges()
        throws SyncException
    {
        List<String>    comps = mRegistryHelper.getChangedConfigComponents();
        
        for (String comp : comps)
        {
            setComponentState(getComponentLifeCycle(comp), LifeCycleMBean.STARTED);
        }
    }
    
    /**
     * Shutdown any old components.
     * @throws SyncException if anything bad happens
     */
    private void shutdownOldComponents()
        throws SyncException
    {
        List<String>    comps = mRegistryHelper.getOldComponents();
        
        for (String comp : comps)
        {
             setComponentState(getComponentLifeCycle(comp), LifeCycleMBean.SHUTDOWN);
        }
    }

    /**
     * Shutdown all components.
     * @throws SyncException if anything bad happens
     */
    private void shutdownAllComponents()
        throws SyncException
    {
        List<String>    comps;
        
        try
        {
            comps = mRegistry.getComponentQuery().getComponentIds(ComponentType.BINDINGS_AND_ENGINES);            
        } 
        catch (RegistryException rEx)
        {
            throw new SyncException(mTranslator.getString(LocalStringKeys.JBI_SYNC_FAILED,
                rEx));           
        }
        
        for (String comp : comps)
        {
            if (!getComponentLifeCycle(comp).equals(LifeCycleMBean.SHUTDOWN))
            {
                setComponentState(getComponentLifeCycle(comp), LifeCycleMBean.SHUTDOWN);                
            }
        }   
    }

   /**
     * Shutdown any components being replaed
     * @throws SyncException if anything bad happens
     */
    private void shutdownReplacedComponents()
        throws SyncException
    {
        List<String>    comps = mRegistryHelper.getReplacedComponents();
        
        for (String comp : comps)
        {
             setComponentState(getComponentLifeCycle(comp), LifeCycleMBean.SHUTDOWN);
        }
    }

    /**
     * Shutdown any components effected by other changes.
     * @throws SyncException if anything bad happens
     */
    private void shutdownAffectedComponents()
        throws SyncException
    {
        List<String>    comps = mRegistryHelper.getAffectedComponents();
        
        for (String comp : comps)
        {
             setComponentState(getComponentLifeCycle(comp), LifeCycleMBean.SHUTDOWN);
        }
    }
        
    /**
     * Get the name of the ComponentLifeCycleMBean for a component name.
     * @param compName - name of the component
     * @return ObjectName containing the ComponentLifeCycleMBean name
     */
    private ObjectName getComponentLifeCycle(String compName)
    {
        ObjectName      name;
        
        name = mCompLifeCycle.get(compName);
        if (name == null)
        {
            name = mMgrCtx.getAdminServiceHandle().getComponentByName(compName);
        }
        return (name);
    }
    
    private String  mConfigTemplate;
    private String  mConfigPath;
    
    private void propertyHack(String compName)
    {
        if (compName.equals("sun-http-binding"))
        {       
            mConfigPath = mRegistryHelper.getNewComponentInfo("sun-http-binding").getWorkspaceRoot() + File.separator + "config.properties";
            setHttpSoapDefaultPorts(readHttpSoapConfig());
        }           
    }
    
    /** Read the httpsoap BC config into a Properties object.
     *  @return Properties object containing httpsoap BC configuration.  This
     *  object will be empty if the config file was not found.
     */
    private Properties readHttpSoapConfig()
    {
        Properties      props = new Properties();
        FileInputStream fis = null;
        
        mConfigTemplate = mEnvCtx.getJbiInstallRoot() + File.separator + "lib" + File.separator +
                        "install" + File.separator + "templates" + File.separator + "config.properties";
        try
        {
            fis = new FileInputStream(mConfigTemplate);
            props.load(fis);
        }
        catch (java.io.IOException ioEx)
        {
            mLogger.fine(mTranslator.getString(
                    LocalStringKeys.JBI_SYNC_HTTP_SOAP_CONFIG_READ_ERROR, 
                    mConfigTemplate, ioEx.getMessage()));
        }
        finally
        {
            if (fis != null)
            {
                try
                {
                    fis.close();
                }
                catch (java.io.IOException ioEx) {}
            }
        }
        
        return props;                
    }
    
    
    /** Names of the http and https listeners in domain.xml. */
    private static final String HTTP_LISTENER  = "http-listener-1";
    private static final String HTTPS_LISTENER = "http-listener-2";
    private static final int PORT_OFFSET = 1000;
    private static final String HTTP_TOKEN     = "${HttpDefaultPort}";
    private static final String HTTPS_TOKEN    = "${HttpsDefaultPort}";
 
    /** Replaces tokens in the httpsoap BC configuration file and writes a new
     *  copy into the component's workspace.
     *  @param props Properties object containing config values to be serialized
     *  into configuration file.
     */
    private void setHttpSoapDefaultPorts(Properties props)
    {
        HttpService     httpService;
        HttpListener    httpListener;
        HttpListener    httpsListener;
        
        // query appserver config for default http/https port values
        try
        {
        	httpService = Globals.getDefaultHabitat().getComponent(HttpService.class);
            httpListener    = httpService.getHttpListenerById(HTTP_LISTENER);
            httpsListener   = httpService.getHttpListenerById(HTTPS_LISTENER);
        }
        catch (Exception ex)
        {
            mLogger.warning(mTranslator.getString(
                    LocalStringKeys.JBI_SYNC_APPSERVER_CONFIG_ERROR, 
                    mConfigTemplate, ex.getMessage()));
            
            return;
        }

        // bump the values by 100 for the SOAP BC
        int httpPort = Integer.parseInt(httpListener.getPort()) + PORT_OFFSET;
        int httpsPort = Integer.parseInt(httpsListener.getPort()) + PORT_OFFSET;
        
        // search and replace in the properties object
        for (Map.Entry entry : props.entrySet())
        {
            if (entry.getValue().equals(HTTP_TOKEN))
            {
                entry.setValue(String.valueOf(httpPort));
            }
            else if (entry.getValue().equals(HTTPS_TOKEN))
            {                
                entry.setValue(String.valueOf(httpsPort));
            }
        }
        
        // write the new properties file
        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(mConfigPath);
            props.store(fos, null);
        }
        catch (java.io.IOException ioEx)
        {
            mLogger.warning(mTranslator.getString(
                    LocalStringKeys.JBI_SYNC_HTTP_SOAP_CONFIG_WRITE_ERROR, 
                    mConfigPath, ioEx.getMessage()));
        }
        finally
        {
            if (fos != null)
            {
                try
                {
                    fos.close();
                }
                catch (java.io.IOException ioEx) {}
            }
        }

    }

}
