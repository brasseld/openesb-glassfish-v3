package org.openesb.glassfish3.container;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.glassfish.api.Startup;
import org.glassfish.api.event.EventListener;
import org.glassfish.api.event.Events;
import org.jvnet.hk2.annotations.Inject;
import org.jvnet.hk2.annotations.Scoped;
import org.jvnet.hk2.annotations.Service;
import org.jvnet.hk2.component.PostConstruct;
import org.jvnet.hk2.component.Singleton;

/**
 * This is the bootstrap class for the JBI runtime framework. It implements the
 * Glassfish AppServer <CODE>Startup</CODE> interface and is responsible for
 * creating the class loader hierarchy required by the JBI runtime. It also acts
 * as a pass-through to the top-level class of the JBI runtime framework,
 * <CODE>SunASJBIFramework</CODE>, which is responsible for all of the
 * actual processing of the events from the AppServer.
 * 
 * @author David BRASSELY
 */
@Service
@Scoped(Singleton.class)
public class JBIStartupModule implements Startup, PostConstruct {

	/**
	 * Class loader for the JSR208 interfaces.
	 */
	private URLClassLoader mJbiClassLoader;

	/**
	 * Class loader for the JBI runtime framework.
	 */
	private URLClassLoader mRuntimeClassLoader;

	/**
	 * Name of the jbi lib directory under jbi home.
	 */
	private static final String JBI_LIB = "lib";

	private static final String GLASSFISH3_JBI_JAR_NAME = "openesb-glassfish3-jbi.jar";
	
	/**
	 * Name of the jar file containing the JSR208-defined interfaces.
	 */
	private static final String JBI_JAR_NAME = "jbi.jar";

	/**
	 * Name of the jar file containing JBI runtime interfaces accessible to some
	 * components.
	 */
	private static final String JBI_EXT_JAR_NAME = "jbi-ext.jar";

	/**
	 * Name of the jar file containing the JBI runtime implementation.
	 */
	private static final String JBI_FRAMEWORK_JAR_NAME = "jbi_framework.jar";

	/**
	 * Name of the top-level class of the JBI runtime framework.
	 */
	private static final String JBI_FRAMEWORK_CLASS_NAME = "org.openesb.glassfish3.jbi.SunASJBIFramework";

	/**
	 * Glassfish Instance Root folder system variable
	 */
	private static final String INSTALL_ROOT = "com.sun.aas.installRoot";
	
	@Inject
    private Logger logger;
	
	@Inject
    private Events events;
	
	/* (non-Javadoc)
	 * @see org.glassfish.api.Startup#getLifecycle()
	 */
	public Lifecycle getLifecycle() {
		return Startup.Lifecycle.SERVER;
	}
	
	/* (non-Javadoc)
	 * @see org.jvnet.hk2.component.PostConstruct#postConstruct()
	 */
	public void postConstruct() {
		logger.fine("Starting JBI runtime module...");
		
		try {
			setUpClassLoaders();
		} catch (Exception e) {
			logger.warning("Unable to initialize JBI classloader : " + e.getMessage());
			throw new IllegalStateException("Unable to initialize JBI classloader : " + e.getMessage());
		}
		
		try {
			logger.fine("Loading JBI framework...");
			EventListener glassfishEventListener = getJBIFramework();
			
			logger.fine("Loading JBI framework... Done ! Listening for events.");
			
			// Registering the listener
	        events.register(glassfishEventListener);
		} catch (Exception e) {
			logger.warning("Unable to load JBI framework : " + e.getMessage());
			throw new IllegalStateException("Unable to load JBI framework : " + e.getMessage());
		}    
	}

	/**
	 * Private method that sets up the class loader hierarchy for the JBI
	 * runtime. Two classloaders are created, one for all of the JSR208-defined
	 * interfaces, and one for the JBI runtime implementation. The reason for
	 * separate class loaders for these two sets of classes is that the JBI
	 * runtime implementation must be isolated from being accessed directly by
	 * installed components. Furthermore, both components and the JBI runtime
	 * must have access to the JSR208 interfaces through the same class loader.
	 * 
	 * These class loaders are set up as follows:
	 * 
	 * - The JSR208 class loader has the AppServer's common class loader as its
	 * parent - The JBI runtime class loader has the JSR208 class loader as its
	 * parent.
	 * 
	 * For information on the AppServer class loader hierarchy:
	 * 
	 * @see http://docs.sun.com/source/819-0079/dgdeploy.html#wp58491
	 * 
	 * @param ctxProperties
	 *            the properties received from the Appserver in the
	 *            LifecycleEvent.
	 * @throws MalformedURLException
	 *             If the URL to any jar file is invalid.
	 * @throws SecurityException
	 *             If access to a class loader was denied.
	 */
	private void setUpClassLoaders() throws Exception,
			java.net.MalformedURLException {
		List<String> jarFiles;
		URL[] jarURLs;

		String installRoot = System.getProperty( INSTALL_ROOT );
		String jbiRoot = installRoot + File.separator + "jbi";
		String jarRoot = jbiRoot + File.separator + JBI_LIB;

		// Create the class loader for the JSR208 interfaces. The parent of
		// this class loader is the OSGI module class loader.

		jarFiles = new ArrayList<String>();
		jarFiles.add(jarRoot + File.separator + JBI_JAR_NAME);
		jarFiles.add(jarRoot + File.separator + JBI_EXT_JAR_NAME);
		jarURLs = list2URLArray(jarFiles);

		mJbiClassLoader = new URLClassLoader(jarURLs, this.getClass().getClassLoader());

		// Create the class loader for JBI runtime. The parent of this class
		// loader is the JSR208 interface class loader.

		jarFiles = new ArrayList<String>();
		jarFiles.add(jarRoot + File.separator + GLASSFISH3_JBI_JAR_NAME);
		jarFiles.add(jarRoot + File.separator + JBI_FRAMEWORK_JAR_NAME);
		jarURLs = list2URLArray(jarFiles);

		mRuntimeClassLoader = new URLClassLoader(jarURLs, mJbiClassLoader);
	}

	/**
	 * Private method to convert a List into a URL array.
	 * 
	 * @param paths
	 *            list of String elements representing paths and JAR file names
	 * @return java.net.URL[] array representing the URLs corresponding to the
	 *         paths, or null if the list is null or if there is an exception
	 *         creating the array.
	 * @throws MalformedURLException
	 *             If any URL is invalid.
	 */
	private URL[] list2URLArray(List<String> paths) throws java.net.MalformedURLException {
		URL[] urls = new URL[paths.size()];
		int i = 0;
		for (String path : paths) {
			File f = new File(path);
			urls[i++] = f.toURI().toURL();
		}

		return urls;
	}

	/**
	 * Private method to load the top-level class of the JBI runtime framework,
	 * create an instance of it, and return a reference to that instance.
	 * 
	 * @return an instance of the top-level runtime class.
	 * @throws Exception
	 *             If the class cannot be loaded.
	 */
	private EventListener getJBIFramework() throws Exception {
		return (EventListener) mRuntimeClassLoader.loadClass(JBI_FRAMEWORK_CLASS_NAME).newInstance();
	}
}
